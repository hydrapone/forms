$(function() {
    $("form").submit(function(e) {
        e.preventDefault();
        $("#submit").hide();
        var email = $("#mail").val();
        var login = $("#login").val();
        var password = $("#pass").val();
        var data = 'mail=' + email + '&login=' + login + '&pass=' + password;
        if(data) {
            $.ajax({
                type: "POST",
                url: "config.php",
                data: data,
                success: function(respons){
                    respons = JSON.parse(respons);
                    $.each(respons, function (key, val) {
                        $("#return_"+key).val(val);
                    });    
                }
            });
            CleanForm();
            $('.res').append("Сообщение отправлено!");
            $('.res').show();            
        } else{
            CleanForm();
            $('.res').append("Ошибка!");
            $('.res').show();
        }
        return false;
    });
});

function CleanForm(){
    $("#mail").val('');
    $("#login").val('');
    $("#pass").val('');
}